<?php

namespace DonnezOrg\SellsyBundle\DependencyInjection;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\{ContainerBuilder, Loader\XmlFileLoader};
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

class SellsyExtension extends ConfigurableExtension
{
    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function loadInternal(array $mergedConfig, ContainerBuilder $container)
    {
        $loader = new XmlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.xml');

        $definition = $container->getDefinition('donnez_org_sellsy_client.client');
        $definition->setArgument(0, $mergedConfig['client_id']);
        $definition->setArgument(1, $mergedConfig['client_secret']);
    }
}
