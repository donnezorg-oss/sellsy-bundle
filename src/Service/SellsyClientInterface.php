<?php

namespace DonnezOrg\SellsyBundle\Service;

use DonnezOrg\SellsyClient\Endpoint\{Companies, Contacts, Opportunities};

interface SellsyClientInterface
{
    public function companies(): Companies;

    public function contacts(): Contacts;

    public function opportunities(): Opportunities;
}
