# Sellsy bundle

This bundle allows integration of the [donnezorg/sellsy-client](https://gitlab.com/donnezorg-oss/sellsy-client/) package
with
symfony.

## Installation

### Install the bundle with composer

Add the needed gitlab repositories

```shell
composer config repositories.sellsy-bundle git https://gitlab.com/donnezorg-oss/sellsy-bundle.git && /
composer config repositories.sellsy-client git https://gitlab.com/donnezorg-oss/sellsy-client.git
```

Require the package

```shell
composer require donnezorg/sellsy-bundle
```

### Create the config file

```yml
# config/packages/sellsy.yaml
sellsy:
    client_id: your generated client id
    client_secret: your generated client secret
```

### Enable the bundle

```php
// config/bundles.php
return [
    ...
    DonnezOrg\SellsyBundle\SellsyBundle::class => ['all' => true]
];
```

## Usage

Inject the client in your controller

```php
use DonnezOrg\SellsyBundle\Service\SellsyClientInterface;

class MyController extends AbstractController {
    public function __construct(private SellsyClientInterface $client) {
    }
}
```

Call an endpoint

```php
public function update(int $id, MyObject $myObject): void {
    $this->client->companies()->update($id, 
        (new CompanyMutator())
            ->setName($myObject->getName())
            ->setSocial(
                (new Social())
                    ->setTwitter("https://twitter.com/...")
            )
    );
}
```


