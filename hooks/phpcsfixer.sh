#!/usr/bin/env bash

echo "> php-cs-fixer"

PHP_CS_FIXER="vendor/bin/php-cs-fixer"
CONFIG_FILE=".php-cs-fixer.dist.php"

if [ -x $PHP_CS_FIXER ]; then
    CHANGED_FILES=$(git diff --cached --name-only --diff-filter=ACM -- '*.php')
    if [ -n "$CHANGED_FILES" ]; then
        $PHP_CS_FIXER fix --config $CONFIG_FILE --ansi $CHANGED_FILES 2>&1
        git add $CHANGED_FILES
    else
        echo "Nothing to check"
    fi
else
    echo ""
    echo "Please install php-cs-fixer, e.g.:"
    echo ""
    echo "  composer require --dev friendsofphp/php-cs-fixer:dev-master"
    echo ""
    exit 1
fi


